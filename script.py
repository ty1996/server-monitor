import csv
import datetime
import requests #Requires install
import time

def main(): #The script is contained in a function in order for it to call and maintain itself

	while True: #Infinite loop to monitor time to execute script every 15 minutes past the hour

		date = datetime.datetime.now() #Sets current time and day to a variable

		if date.minute != 15:
			# print("Not 15 minutes. Waiting before checking again.")
			time.sleep(30) #Waits for 30 seconds before checking the minutes so as to use less resources
			continue
		elif date.minute == 15:
			print("15 minutes. Pulling data.")
			break

	if date.month < 10: #Add a leading zero if month is less than 10
		month = "0" + str(date.month)
	else:
		month = str(date.month)

	if date.day < 10: #Add a leading zero if day is less than 10
		day = "0" + str(date.day)
	else:
		day = str(date.day)

	if date.hour < 10: #Add a leading zero if hour is less than 10
		hour = "0" + str(date.hour)
	else:
		hour = str(date.hour)

	#####

	filename = month + "_" + day + "_" + hour
	url = #REDACTED

	with requests.Session() as s: #Connect to website and pull data
		download = s.get(url)
		decoded_content = download.content.decode('utf-8')
		cr = csv.reader(decoded_content.splitlines(), delimiter=',')
		my_list = list(cr)

	#####

	output_file = "sensordata_2018_" + filename + ".csv"

	with open(output_file, 'w', newline='') as csvfile: #Initiate CSV file
		wr = csv.writer(csvfile)
		wr.writerow(my_list[0]) #CSV Header

		for row in my_list[1:]: #Loop through the data
			if float(row[3]) < 5.0:
				# print("True")
				wr.writerow(row) #Puts all sensors under 5.0 in to a CSV file

	#####

	with open(output_file, 'rb') as f:
		r = requests.post(#REDACTED, headers={"Authorization":#REDACTED},files={'sensor_output.csv': f})

	time.sleep(3480)

	main() #Loop function to run again

print("Script running.")
main() #Executes script the first time

